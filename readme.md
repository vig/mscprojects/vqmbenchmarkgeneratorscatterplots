# Scagnostics benchmark generator

Scagnostics captures the visual presence of nine visual metrics. With the benchmark generator, we can capture and visualise the high dimensional space of Scagnostics into a single benchmark dataset. Furthermore, the benchmarks can be created in various ways due to the configurations of the method. 
The [thesis defence slides](https://git.science.uu.nl/vig/mscprojects/vqmbenchmarkgeneratorscatterplots/-/tree/Final_version/thesis) provide a quick overview of our method.
A detailed overview of our research is found in the [master thesis](https://git.science.uu.nl/vig/mscprojects/vqmbenchmarkgeneratorscatterplots/-/tree/Final_version/thesis).

## Visuals

### Flowchart of newly developed method
<img src="used_images/test.png"  width="50%" height="50%">


### Parallel coordinate of benchmark dataset
<img src="used_images/par_complete.png"  width="70%" height="70%">

### Biplot of benchmark dataset
<img src="used_images/pca_new_data.png"  width="70%" height="70%">

### Distributions of Scagnostics
<img src="used_images/distribution_25.png"  width="100%" height="100%">

## Installation

#### Install requirements
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the requirements.txt.


```bash
pip install requirements.txt
```
The method is built using Pythion version 3.7 due to some restrictions\issues in the Scagnostics Python package. However, we assume that other Python versions also work if you set the Scagnoticversion in the config file to R.

## Usage Explanation

#### Configuration of method
The program/config.json can adjust the process of the benchmark generator in a variety of ways. As JSON files do not allow comments, we will shortly explain how the config file variables adjust the process.

```python
{
"Number_of_values_per_metric": 50, #The number of values per metric (Section 3.2.1) 
"Starting_Metric_Value": 0, #Can be used to filter lower direction values
"Ending_Metric_Value": 1, #Can be used to filter higher direction values
"metrics": "All", #A scatterplot can be created for a specif metric or all metrics
"Single_metric_generator": "True", #If this value is set to True, it takes all
#the previous metrics one at the time. If set to false, it creates directions with
#combinations of metrics (Section 3.2.1 (Single metric or Multimetric lists))


"starting_position": "Random", #Random or Previous (Section 3.2.2)
"Number_of_points_in_starting_position_min": 5, #Minimal number of points in plot
"Number_of_points_in_starting_position_max": 50, #Maximal number of points in plot


"maxIterations": 3000, #Number of iterations the method has to create a direction. 
"permutationsize": 0.1, #Percentage of change of the datapoints (Section 3.2.3)
"Optimizer": "Annealing", #Annealing or Random optimizer (Section 4.1.6)


"repeatprocess":1, #The selected directions can be looped multiple times
"Scagnoticversion": "R", #R or Python. Package used to calculate Scagnistics


"savedata": "True", #Could be set to False for testing.
"showstatus": "False", #Provides additional  information in the logg if set to True.
"silent": "False", #If set to True, little to no information is given in the logg.

"Adjust_striated": "False", #Unique approach to create high striated scatterplots
}
```

### Stored data
Data is stored using .pkl files. These files can be opened using pandas: `pd.read_pickle('LocationOfFile.pkl')`. The data is stored in three different directories.
Make sure that all the directiories are empy before you execute the program.

#### Scatterplot images
All the scatterplot images are saved to the [scatterplot folder](BenchmarkGenerator/program/generated_data/scatterplots). The scatterplots are automatically divided into sub-directions based on the Scagnostic metric(s) that have been used for the process.
#### Primary dataset
The [primary_dataset folder]("BenchmarkGenerator/program/generated_data/primary_dataset") hold the information about the scatterplots that have been saved. Each metric is given its data frame, which is stored using a .pkl file. All the data frames can be easily combined into a single data frame using the [view_data]("BenchmarkGenerator/program/view_data.py") script.
All the data frames are combined into the "complete" data frame. 
#### Data of scatterplot
The scatterplots are created by adjusting a dataset consisting of two arrays. If a scatterplot is saved to the benchmark dataset, we also save the two arrays that create this scatterplot into this [folder]("program/generated_data/data_scatterplot") so that scatterplots can be recreated or adjusted.
## Basic Usage example
Let us create a benchmark dataset for Clumpy and Outlying metrics by creating using 200 values per individual metric. The scatterplots should contain 75 scatterplot points.
The following settings are set as configuration:
```python
"Number_of_values_per_metric": 200,
"metrics": "Clumpy Outlying"
"Number_of_points_in_starting_position_min": 75, 
"Number_of_points_in_starting_position_max": 75,
```

Now we can execute our method by running the [Main]("BenchmarkGenerator/program/main.py") file. The method will try to compute a target list with in total 2*200=400 scatterplots. 
At the end of the execution, we receive a short overview of how the method performed and how many scatterplots of the list we computed:
```python
Total number of scatter plots tried: 400
Number of times failed: 53
Number of times succeeded: 357
Lenght of execution in seconds: 3200
 ```

The complete data frame of all scatterplots can be created and viewed using the [view_data]("BenchmarkGenerator/program/view_data.py") script. This automatically shows the parallel coordinate of the 357 scatterplots and can be used to filter specific scagnostic ranges.
<img src="used_images/par_outlying.png"  width="50%" height="50%">
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please contact Bram Dijkers for aditional questions (Bramdijkers@outlook.com) 