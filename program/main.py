#Maybe needed for some errors
from __future__ import division
from filter import filter
from pipeline import get_value
from config_manager import load_configurations
import os.path
import itertools
import pickle
import pandas as pd
import datetime as dt
from os import path
from random import randrange


def main():
 #Function to calculate the duration of the entire loop
 start_time = dt.datetime.today().timestamp()
 configurations = load_configurations("program/config.json")

 #Load the metrics for the Single_metric_generator
 Multi_metric_generator="False"
 if configurations["Single_metric_generator"]=="True":
    if configurations["metrics"] == 'All':
        metrics=['Clumpy', "Outlying", "Skewed", "Sparse", "Striated", "Convex", "Skinny", "Stringy", "Monotonic"]
    else:
        metrics = configurations["metrics"]
        metrics = metrics.split(" ")
 else:
     Multi_metric_generator="True"

 #Counting
 count = 0
 skipped_scatterplots = 0
 # Load the metrics for the Multi_metric_generator
 if Multi_metric_generator =="True":
    if configurations["metrics"] == 'All':
        metrics = ['Clumpy', "Outlying", "Skewed", "Sparse", "Striated", "Convex", "Skinny", "Stringy", "Monotonic"]
    else:
        metrics = configurations["metrics"]
        metrics = metrics.split(" ")

 for i in metrics:
    if Multi_metric_generator=="True":
         configurations = load_configurations("program/config.json")
         metrics = configurations["metrics"]
    else:
        metrics= i

    #Delete old datasets that can be found in location where is saved
    #savelocation = configurations["savelocation"]
    #testfile = path.exists(f"permutation/data_scatterplot/{savelocation}/df/{metrics}.pkl")
    #if testfile==True:
    #    os.remove(f"permutation/data_scatterplot/{savelocation}/df/{metrics}.pkl")
    #    print("An old dataset is deleted")

    #Random
    names=metrics.split()
    numberofnames=len(names)
    avg_itertaitions = []
    memory=0

    #Used for reporting the number of fails and successes
    faileddirections = []
    succesdirections= []

    #Loading the configuration variables
    permutationsize=configurations["permutationsize"]
    repeatprocess = configurations["repeatprocess"]
    maxIterations = configurations["maxIterations"]
    Optimizer = configurations["Optimizer"]
    numberofclasses = configurations["Number_of_values_per_metric"]

    #Creates equal directions over the number of groups
    classes= []
    sizeofclass=round(1/numberofclasses,5)
    mycount=0
    for i in range(numberofclasses):
        if mycount==0:
            classes.append(0)
            mycount= mycount+1
        if mycount==numberofclasses:
            classes.append(1)
        else:
            mycount = mycount + 1
            classes.append(sizeofclass)
            sizeofclass=round(sizeofclass+(1/numberofclasses), 5)
    if configurations["silent"] != 'True':
        print(f"Classes used: {classes}")
    #Give each loop a unique processnumber, this way each image gets a unique id
    processnumber=0
    #Creates multiple loops to increase the amount of data_scatterplot, (only) usefull for single metric generators
    for i in range(repeatprocess):
        succes = False

        numpoints_min = configurations["Number_of_points_in_starting_position_min"]
        numpoints_max = configurations["Number_of_points_in_starting_position_max"]
        if numpoints_min == numpoints_max:
            irand = numpoints_min
        else:
            irand = randrange(numpoints_min, numpoints_max)
        #print(irand)
        processnumber=processnumber+1

        #Creates the differnt direction values
        for p in itertools.product(classes,repeat=numberofnames): #creates all possible combinations
            #Creates the direction by combining the metric with a direction value
            direction = dict(zip(names, p))  # the fit


            breakloop=False
            for name, value in direction.items():
                if configurations["Starting_Metric_Value"]  > value:
                    breakloop = True
                    break
                if configurations["Ending_Metric_Value"]  <  value:
                    breakloop = True
                    break
                if breakloop == True:
                    skipped_scatterplots = skipped_scatterplots + 1
                    break
            if breakloop == True:
                skipped_scatterplots = skipped_scatterplots + 1
                continue

            count = count + 1
            #Here we start the creation of the actual dateset and scatterplot and start using the direction
            time, status, direction, memory = get_value(configurations, direction=direction,memory=memory, processnumber=processnumber, metrics=metrics, succes=succes, irand=irand)



            #Saving lists of failed and succes directions
            if status=="succes":
                succes=True
                print("")
                print(f"Succesfully created a scatterplot with the following direction: {direction}")
                dictionary_copy = str(direction)
                # succesdirections.append(dictionary_copy)
                avg_itertaitions.append(time)
                exist = path.exists(f"program/generated_data/direction_list/{metrics}_succes.pkl")
                PICKLE_FILE= (f"program/generated_data/direction_list/{metrics}_succes.pkl")
                if exist == False:
                    file_name = (f"program/generated_data/direction_list/{metrics}_succes.pkl")
                    open_file = open(file_name, "wb")
                    list=[]
                    list.append(dictionary_copy)
                    pickle.dump(list, open_file)
                    open_file.close()
                if exist == True:
                    directionlist = pd.read_pickle(f"program/generated_data/direction_list/{metrics}_succes.pkl")
                    #dict.dictionary_copy()
                    series_obj = pd.Series(dictionary_copy)
                    directionlist.append(dictionary_copy)
                    with open(f"program/generated_data/direction_list/{metrics}_succes.pkl", 'wb') as f:
                        pickle.dump(directionlist, f)
            if status=='fail':
                print(f"Unable to create the following direction {direction}")
                dictionary_copy = str(direction)
                faileddirections.append(dictionary_copy)
                exist = path.exists(f"program/generated_data/direction_list/{metrics}_fail.pkl")
                PICKLE_FILE = (f"program/generated_data/direction_list/{metrics}_fail.pkl")
                if exist == False:
                    file_name = (f"program/generated_data/direction_list/{metrics}_fail.pkl")
                    open_file = open(file_name, "wb")
                    list = []
                    list.append(dictionary_copy)
                    pickle.dump(list, open_file)
                    open_file.close()
                if exist == True:
                    directionlist = pd.read_pickle(f"program/generated_data/direction_list/{metrics}_fail.pkl")
                    #dict.directionlist()
                    directionlist.append(dictionary_copy)
                    #directionlist = directionlist.append(series_obj, ignore_index=True)
                    with open(f"program/generated_data/direction_list/{metrics}_succes.pkl", 'wb') as f:
                        pickle.dump(directionlist, f)

            print(f"Number of scatter plots tried: {count}")
            print()
            print()



 #avg_value = sum(avg_itertaitions) / len(avg_itertaitions)
 #print(f"Average tries per succesfull scatterplot {avg_value}")
 print(f"Total number of scatter plots tried {count}")
 print(f"Number of times failed: {len(faileddirections)}")
 succesdirections = count - len(faileddirections)
 print(f"Number of times succeeded: {succesdirections}")
 print(f"maxIterations: {maxIterations}")
 print(f"permutationsize: {permutationsize}")
 print(f"Optimizer: {Optimizer}")
 print(f"skipped number: {skipped_scatterplots}")
 end_time = dt.datetime.today().timestamp()
 diffs = (end_time - start_time)
 print(f"Lenght of execution in seconds: {diffs}")


 f = open('permutation/data_scatterplot/combination/timespend.pckl', 'wb')
 pickle.dump(diffs, f)
 f.close()





main()

