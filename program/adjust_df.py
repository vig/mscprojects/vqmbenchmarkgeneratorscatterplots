#Short funtion that adjusts the scatterplot/dataframe to the permutated one
def adjustdf(Adjusted_X, Adjusted_Y, AdjustedMeasures, AdjustedError):
    CurrentArray_X = Adjusted_X
    CurrentArray_Y = Adjusted_Y
    CurrentMeasures = AdjustedMeasures
    CurrentError = AdjustedError
    return CurrentArray_X, CurrentArray_Y, CurrentMeasures, CurrentError