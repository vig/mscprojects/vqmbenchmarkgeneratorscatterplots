from error import error
from perturb import moveRandomPoints
from checkrange import isInFilterRange
from adjust_df import adjustdf
from utils import SimulatedAnnealing
from get_scagnostics import getscagnostics
from save_data import savethedata
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
import datetime as dt
from numpy.random import randn
from random import randrange


# A small function that can create a randoms scatterplot with irand as the number of points
def getdf(irand):
    x1 = np.random.randint(1, 100, irand)
    y1 = np.random.randint(1, 100, irand)

    return x1, y1


# Can be used to load a specif dataset as a starting position
def loaddata():
    OriginalArray_X = np.load('x1.npy')
    OriginalArray_Y = np.load('y1.npy')

    return OriginalArray_X, OriginalArray_Y


# The main function that adjusts scatterplots
def get_value(configurations, direction, memory, processnumber, metrics, succes, irand):
    # set the configurations
    permutationsize = configurations["permutationsize"]
    optimizer = configurations["Optimizer"]
    maxIterations = configurations["maxIterations"]
    savedata = configurations["savedata"]
    showstatus = configurations["showstatus"]
    starting_position = configurations["starting_position"]
    Scagnoticversion = configurations["Scagnoticversion"]
    numpoints_min = configurations["Number_of_points_in_starting_position_min"]
    numpoints_max = configurations["Number_of_points_in_starting_position_max"]
    numberofclasses = configurations["Number_of_values_per_metric"]
    silent = configurations["silent"]

    # Calculates the margin allowed in the error
    sizeofclass = 1 / numberofclasses
    newclass = sizeofclass
    rangemargin = newclass / 2

    # Sets the number of points in the scatterplot

    # Takes ending position of the last succesfull scatterplot to reduce 'dubble' workload.
    if starting_position == "Previous":
        # print(succes)
        for key, value in direction.items():
            if value != 0 and succes == True:
                OriginalArray_X = np.load('CurrentArray_X.npy')
                OriginalArray_Y = np.load('CurrentArray_Y.npy')
                irand = len(OriginalArray_Y)


            # It creates a new scatterplot if a new scagnostic metric is initiated
            else:
                OriginalArray_X, OriginalArray_Y = getdf(irand)

    # Loads an unique oploaded dataset
    if starting_position == "Load":
        OriginalArray_X, OriginalArray_Y = loaddata()  # Load data_scatterplot
        if showplots == "True":
            plt.scatter(OriginalArray_X, OriginalArray_Y)
            plt.show()

    # Loads a new and random scatterplot as starting position
    if starting_position == "Random":
        OriginalArray_X, OriginalArray_Y = getdf(irand)

    # Calculates how much time a single direction has taken
    last_time = dt.datetime.today().timestamp()
    diffs = []

    # Original Positions
    OriginalMeasures = getscagnostics(OriginalArray_X, OriginalArray_Y, Scagnoticversion)
    OriginalError = error(qmresult=OriginalMeasures, direction=direction)

    # Set the original dataset to the current dataset
    CurrentArray_X = OriginalArray_X  # setting the starting point to the current df/scatterplot
    CurrentArray_Y = OriginalArray_Y

    # To see how many iterations have been used
    time = 0

    # If this turn True, the loop is stopped and the data_scatterplot is saved
    inRange = False

    if silent != 'True':
        print(f"We are currentlt training for: {direction}")

    while not inRange:

        # Again calculate the starting position
        CurrentMeasures = getscagnostics(CurrentArray_X, CurrentArray_Y, Scagnoticversion)
        CurrentError = error(qmresult=CurrentMeasures, direction=direction)
        if silent != 'True':
            printerror = 1+CurrentError
            print(f"Current Fitness at start: {printerror}")

        # Used to accept worse solution if stuck in local minimum.
        simAnnealing = SimulatedAnnealing(maxIterations, CurrentError)

        for i in tqdm(range(maxIterations)):
            # Checks how much iterations have been used
            time = time + 1

            # Random technique
            if optimizer == 'Random':
                Adjusted_X = np.random.randint(1, 100, irand)
                Adjusted_Y = np.random.randint(1, 100, irand)
                AdjustedMeasures = getscagnostics(Adjusted_X, Adjusted_Y, Scagnoticversion)
                AdjustedError = error(qmresult=AdjustedMeasures,
                                      direction=direction)

                # Annealing
            if optimizer == 'Annealing':
                # The dataset/scatter plot is adjusted
                Adjusted_X, Adjusted_Y = moveRandomPoints(permutationsize, domainMin=0, domainMax=100,
                                                          OriginalArray_Y=CurrentArray_Y,
                                                          OriginalArray_X=CurrentArray_X, metrics=metrics,
                                                          direction=direction,
                                                          configurations=configurations)  # Function that moves the points into a random direction

                # the new scagnostics and error value is calculated from the adjusted dataset
                AdjustedMeasures = getscagnostics(Adjusted_X, Adjusted_Y, Scagnoticversion)

                AdjustedError = error(qmresult=AdjustedMeasures,
                                      direction=direction)  # calculate how the error is changed

            # If the adjusted error is better (lower) then the old error, we adjust the data_scatterplot
            if CurrentError < AdjustedError:  # If the error is reduced, we accept the knew df/scatterplot
                if showstatus == "True":  # Optimal function to see how the df is adjusted over time
                    print(f" Better. CurrentError: {CurrentError} and the AdjustedError {AdjustedError}")
                CurrentArray_X, CurrentArray_Y, CurrentMeasures, CurrentError = adjustdf(Adjusted_X, Adjusted_Y,
                                                                                         AdjustedMeasures,
                                                                                         AdjustedError)
                # The annealing function could also accept the adjusted dataset
            if simAnnealing.shouldAccept(AdjustedError) and CurrentError > AdjustedError and optimizer == 'annealing':
                if showstatus == "True":
                    print(f" Heated. CurrentError: {CurrentError} and the AdjustedError {AdjustedError}")
                CurrentArray_X, CurrentArray_Y, CurrentMeasures, CurrentError = adjustdf(Adjusted_X, Adjusted_Y,
                                                                                         AdjustedMeasures,
                                                                                         AdjustedError)

                # if old_data =="previous" and AdjustedError<=-0.4:
                #    print("we went too far")
                #    CurrentArray_X, CurrentArray_Y = getdf(irand)

                # the temp is update each iteration, if updated or not
                simAnnealing.updateTemp()

            # filter that checks if the current results are in the range
            inrage = isInFilterRange(silent=silent, qmresult=CurrentMeasures, direction=direction,
                                     maxiterations=maxIterations,
                                     time=time, rangemargin=rangemargin)

            # If the dataset is inrange of the direction, we save the data_scatterplot
            if inrage == True:
                status = "succes"
                if starting_position == "previous":
                    np.save("CurrentArray_X.npy", CurrentArray_X)
                    np.save("CurrentArray_Y.npy", CurrentArray_Y)

                # Save the data_scatterplot
                if savedata == "True":
                    # Calculates how long it has taken to develop the final dataset
                    new_time = dt.datetime.today().timestamp()
                    diffs = (new_time - last_time)
                    savethedata(direction=direction, measures=CurrentMeasures, CurrentArray_X=CurrentArray_X,
                                CurrentArray_Y=CurrentArray_Y, processnumber=processnumber,
                                metrics=metrics, irand=irand, timespend=diffs, iterations=time)


                if silent != 'True':
                    x = ' '  # create empty line in console
                    # print(x)

                return time, status, direction, memory
                break

        # if we do not get Inrange within the max iterations
        if time == maxIterations:
            status = "fail"

            if silent != "True":
                print(f"Fitting failed: The ending position was was: {CurrentMeasures}.")  # show the status

            return time, status, direction, memory
    return time, status, direction, memory


