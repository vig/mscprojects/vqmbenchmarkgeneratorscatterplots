from config_manager import load_configurations
import re
import os.path
import pandas as pd
import matplotlib.pyplot as plt
from pathlib import Path
from os import path
import time
from random import randint

# get current time in seconds


column_names = ['id', 'clumpy', "Outlying", "Skewed", "Sparse", "Striated", "Convex", "Skinny",
                    "Stringy", "Monotonic", 'image_location']
maindf = pd.DataFrame(columns=column_names)

def savethedata(direction, measures, CurrentArray_X, CurrentArray_Y,processnumber,metrics, irand, timespend, iterations):

    column_names = ['id', 'Clumpy', "Outlying", "Skewed", "Sparse", "Striated", "Convex", "Skinny",
                    "Stringy", "Monotonic", 'Number_of_points', 'Image_location', 'Data_location', "Time_Spend", "Saved_iteration", "Method", "Starting_dataset"]
    maindf = pd.DataFrame(columns=column_names)
    #print(f"we save df at: {metrics}")

    #Converst direction to string so it can be used for saving
    dictstring = str(direction)
    string_no_punctuation = re.sub("[^\w\s]", "", dictstring)  # deletes punctuation
    string_no_punctuation= string_no_punctuation.replace('Clumpy ',"Clumpy")
    string_no_punctuation = string_no_punctuation.replace('Outlying ', "Outlying")
    string_no_punctuation = string_no_punctuation.replace('Skewed ', "Skewed")
    string_no_punctuation = string_no_punctuation.replace('Sparse ', "Sparse")
    string_no_punctuation = string_no_punctuation.replace('Striated ', "Striated")
    string_no_punctuation = string_no_punctuation.replace('Convex ', "Convex")
    string_no_punctuation = string_no_punctuation.replace('Skinny ', "Skinny")
    string_no_punctuation = string_no_punctuation.replace('Stringy ', "Stringy")
    string_no_punctuation = string_no_punctuation.replace('Monotonic ', "Monotonic")
    converted_processnumber = str(processnumber)
    id = string_no_punctuation+"-"+converted_processnumber
    #save df with information about dataset
    my_file = Path(f"program/generated_data/primary_dataset/{metrics}.pkl")
    try:
        my_abs_path = my_file.resolve(strict=True)
    except FileNotFoundError:
        maindf.to_pickle(f"program/generated_data/primary_dataset/{metrics}.pkl")  # save the df

    my_file = Path(f"program/generated_data/scatterplots/{metrics}")

    testpath = path.isdir(f"program/generated_data/scatterplots/{metrics}")
    if testpath==False:
        os.mkdir(f"program/generated_data/scatterplots/{metrics}")

    #Information to fill the df
    Clumpy = measures.get("Clumpy")
    Outlying = measures.get("Outlying")
    Skewed = measures.get("Skewed")
    Sparse = measures.get("Sparse")
    Striated = measures.get("Striated")
    Convex = measures.get("Convex")
    Skinny = measures.get("Skinny")
    Stringy = measures.get("Stringy")
    Monotonic = measures.get("Monotonic")

    #setting about the scatterplot
    t = time.time()


    plt.figure(figsize=(1.12, 1.12))
    plt.scatter(CurrentArray_X, CurrentArray_Y, s=1)
    plt.axis('off')
    images_dir = f"program/generated_data/scatterplots/{metrics}"
    image_name = f"{images_dir}/{id}.png"
    value = randint(0, 100)
    plt.savefig(f"{images_dir}/{id}.png", dpi=300)

    plt.clf()

    #Information for the df
    maindf = pd.read_pickle(f"program/generated_data/primary_dataset/{metrics}.pkl")
    configurations = load_configurations("program/config.json")
    method = configurations["Optimizer"]
    Single_metric_generator = configurations["Single_metric_generator"]
    if method=='Annealing':
        if Single_metric_generator == 'True':
            method = "Single-metric generators with simulated annealing "
        else:
            method = "Multi-metric generators with simulated annealing "


    starting_position = configurations["starting_position"]

    # save dataset that has been saved in main dataframe
    column_names = ['x-array', 'y-array', 'id']
    datasetinfo = pd.DataFrame(columns=column_names)
    my_file = Path(f"program/generated_data/data_scatterplot/{metrics}-datasets.pkl")
    try:
        my_abs_path = my_file.resolve(strict=True)
    except FileNotFoundError:
        datasetinfo.to_pickle(f"program/generated_data/data_scatterplot/{metrics}-datasets.pkl")  # save the df
    datasetinfo = pd.read_pickle(f"program/generated_data/data_scatterplot/{metrics}-datasets.pkl")
    series_obj = pd.Series(
        [CurrentArray_X, CurrentArray_X, id],
        index=datasetinfo.columns)
    datasetinfo = datasetinfo.append(series_obj, ignore_index=True)
    datasetinfo.to_pickle(f"program/generated_data/data_scatterplot/{metrics}-datasets.pkl")  # save the df
    name_dataset= (f"program/generated_data/data_scatterplot/{metrics}/{id}")
    #print(f"name image loc:{image_name}")
    #print(f"name ds loc:{name_dataset}")



    #information is loaded to the df
    series_obj = pd.Series(
        [id, Clumpy, Outlying, Skewed, Sparse, Striated, Convex, Skinny, Stringy, Monotonic, irand, image_name, name_dataset, timespend, iterations, method, starting_position ],
        index=maindf.columns)
    maindf = maindf.append(series_obj, ignore_index=True)




    #df is saved in the local environment
    maindf.to_pickle(f"program/generated_data/primary_dataset/{metrics}.pkl")
