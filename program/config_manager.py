import json
import sys

default_configurations = {  # In case the configurations file is missing or incomplete
    "Optimizer": "Annealing",
    "starting_position": "Random",
    "Number_of_points_in_starting_position_min": 25,
    "Number_of_points_in_starting_position_max": 25,
    "Number_of_values_per_metric": 3,
    "maxIterations": 5000,
    "savedata": "True",
    "showstatus": "False",
    "silent": "False",
    "permutationsize": 0.1,
    "metrics": "All",
    "repeatprocess":4,
    "Scagnoticversion": "R",
    "Single_metric_generator": "True",
    "Adjust_striated": "False",
    "Starting_Metric_Value": 0,
    "Ending_Metric_Value": 1
}

def load_configurations(config_file_path) -> dict:
    """Load the configurations from file and use default configurations for missing keys"""
    file = open(config_file_path)
    configurations = json.load(file)

    missing_configurations = {k: v for k, v in default_configurations.items() if k not in configurations}
    #if
    for k, v in missing_configurations.items():
        print(f"Missing '{k}' configuration preference. Using default value '{v}'")
        configurations[k] = v

    #if configurations["Single_metric_generator"]=='True' and configurations["Multi_metric_generator"]=='True':
    #    print("Configuration settings error: can not have both generator on at the same time")
     #   sys.exit(1)

    #if configurations["Multi_metric_generator"]=='True' and configurations["repeatprocess"]>1:
    #    print("Configuration settings error: can not have multi metric generator with multiple repeats")
    x=configurations["Optimizer"]
    if configurations["Optimizer"]!="Random" and configurations["Optimizer"]!="Annealing":
        print(f"Configuration settings error: Optimizer can only be Random or Annealing.")
        sys.exit(1)

    if configurations["starting_position"]!="Random" and configurations["starting_position"]!="Previous":
        print(f"Configuration settings error: Starting_position can only be random or Previous.")
        sys.exit(1)

    if configurations["Starting_Metric_Value"]>1 or configurations["Ending_Metric_Value"]>1:
        print("Configuration settings error: Starting_Metric_Value and Ending_Metric_Value cant be higher than 1.")

    if configurations["Starting_Metric_Value"]<0 or configurations["Ending_Metric_Value"]<0:
        print("Configuration settings error: Starting_Metric_Value and Ending_Metric_Value cant be lower than 0.")

    if configurations["Starting_Metric_Value"]> configurations["Ending_Metric_Value"]:
        print("Configuration settings error: Starting_Metric_Value can not be higher than Ending_Metric_Value.")


    #if configurations["Doall"]=="False" and configurations["repeatprocess"]>1:
    #    print("ERROR: Doall cant be false with multiple repeatprocess")
    #    sys.exit(1)
    return configurations
