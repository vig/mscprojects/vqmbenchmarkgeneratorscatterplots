from pyscagnostics import scagnostics
import rpy2.robjects as robjects
import os

def getscagnostics(Xvalue,Yvalue, Scagnoticversion="R"):

    if Scagnoticversion=="Python" :
        measures, _ = scagnostics(Xvalue, Yvalue, remove_outliers=True)

    if Scagnoticversion == "R" :
        path = ''
        measures = {}
        r_source = robjects.r['source']
        r_source(os.path.join(path, 'program/get_scag.r'))
        r_getname = robjects.globalenv['scags']
        scags = r_getname(robjects.FloatVector(Xvalue), robjects.FloatVector(Yvalue))
        measures['Outlying'] = scags[0]
        measures['Skewed'] = scags[1]
        measures['Clumpy'] = scags[2]
        measures['Sparse'] = scags[3]
        measures['Striated'] = scags[4]
        measures['Convex'] = scags[5]
        measures['Skinny'] = scags[6]
        measures['Stringy'] = scags[7]
        measures['Monotonic'] = scags[8]



    return measures
