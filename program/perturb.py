from random import random, randint
from random import randrange, uniform


def moveRandomPoints(permutationsize, domainMax, domainMin, OriginalArray_X, OriginalArray_Y,metrics,direction, configurations):
    Adjust_striated = configurations["Adjust_striated"]
    rangeDomain = domainMax - domainMin
    maxStep = rangeDomain * permutationsize #how large the change can be
    minStep = -maxStep
    Adjusted_X= []
    Adjusted_Y= []

    for i in OriginalArray_X:
        rand_x = randint(minStep, maxStep)

        if metrics == 'Striated' and Adjust_striated == 'True':

            if direction["Striated"]>0.2 and direction["Striated"]<0.5 :
                irand=randrange(2, 60)
                i = round(i / irand) * irand + 0.000000001
            if direction["Striated"] > 0.5 :
                irand = randrange(10,100)
                i = round(i / irand) * irand + 0.000000001

        if i + rand_x < domainMax and i + rand_x > domainMin:
            i = i + rand_x
            Adjusted_X.append(i)
        else:
            Adjusted_X.append(i)

    for i in OriginalArray_Y:
        rand_y = randint(minStep, maxStep)
        if i + rand_y < domainMax and i + rand_y > domainMin:
            i = i + rand_y
            Adjusted_Y.append(i)
        else:
            Adjusted_Y.append(i)
    return Adjusted_X, Adjusted_Y