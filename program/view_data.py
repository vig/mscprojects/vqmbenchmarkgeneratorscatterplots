import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
#create complete primary_dataset
df1 = pd.read_pickle("program/generated_data/primary_dataset/Clumpy.pkl")
df2 = pd.read_pickle("program/generated_data/primary_dataset/Convex.pkl")
df3 = pd.read_pickle("program/generated_data/primary_dataset/Monotonic.pkl")
df4 = pd.read_pickle("program/generated_data/primary_dataset/Outlying.pkl")
df5 = pd.read_pickle("program/generated_data/primary_dataset/Skewed.pkl")
df6 = pd.read_pickle("program/generated_data/primary_dataset/Skinny.pkl")
df7 = pd.read_pickle("program/generated_data/primary_dataset/Sparse.pkl")
df8 = pd.read_pickle("program/generated_data/primary_dataset/Striated.pkl")
df9 = pd.read_pickle("program/generated_data/primary_dataset/Stringy.pkl")

nan_value = 0
data_frames = [df1, df2, df3, df4, df5, df6, df7, df8, df9]
df = result = pd.concat(data_frames)
df.to_pickle("program/generated_data/primary_dataset/Complete.pkl")


pd.set_option('display.max_columns', None)
print(df)


minpoints= df['Number_of_points'].min()
maxpoints= df['Number_of_points'].max()
mintime= round(df['Time_Spend'].min())
maxtime= round(df['Time_Spend'].max())
miniterations= df['Saved_iteration'].min()
maxiterations= df['Saved_iteration'].max()

fig = go.Figure(data=
    go.Parcoords(
        line = dict(color = df['Number_of_points'],
                   colorscale = 'Electric',
                   showscale = True,
                   cmin = minpoints,
                   cmax = maxpoints),
        dimensions = list([
            dict(range= [0,1],
                 #constraintrange = [100000,150000],
                 label = "Clumpy", values = df['Clumpy']),
            dict(range = [0,1],
                 label = 'Outlying', values = df['Outlying']),
            dict(range = [0,1],
                 #ticktext = ['A','AB','B','Y','Z'],
                 label = 'Sparse', values = df['Sparse']),
            dict(range = [0,1],
                 #tickvals = [0,1,2,3],
                 label = 'Skewed', values = df['Skewed']),
            dict(range = [0,1],
                 #visible = True,
                 label = 'Striated', values = df['Striated']),
            dict(range = [0,1],
                 label = 'Convex', values = df['Convex']),
            dict(range = [0,1],
                 #visible = True,
                 label = 'Monotonic', values = df['Monotonic']),
            dict(range = [0,1],
                 label = 'Stringy', values = df['Stringy']),
            dict(range = [0,1],
                 label = 'Skinny', values = df['Skinny']),
            dict(range = [miniterations,maxiterations],
                 label = 'Saved_iteration', values = df['Saved_iteration']),
            dict(range = [mintime,maxtime],
                 label = 'Time_Spend', values = df['Time_Spend']),
            dict(range = [minpoints,maxpoints],
                 label = 'Number_of_points', values = df['Number_of_points'])])
    )
)
fig.show()

