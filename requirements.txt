absl-py==1.0.0
argcomplete==2.0.0
argon2-cffi==21.3.0
argon2-cffi-bindings==21.2.0
astunparse==1.6.3
attrs==21.4.0
backcall==0.2.0
backports.zoneinfo==0.2.1
bleach==4.1.0
Brotli==1.0.9
cached-property==1.5.2
cachetools==4.2.4
certifi==2021.10.8
cffi==1.15.0
charset-normalizer==2.0.10
click==8.0.4
colorama==0.4.4
cycler==0.11.0
dash==2.3.0
dash-bootstrap-components==1.0.3
dash-core-components==2.0.0
dash-html-components==2.0.0
dash-table==5.0.0
debugpy==1.5.1
decorator==5.1.1
defusedxml==0.7.1
docopt==0.6.2
entrypoints==0.3
Flask==2.0.3
Flask-Compress==1.11
flatbuffers==2.0
fonttools==4.28.5
gast==0.3.3
google-auth==2.3.3
google-auth-oauthlib==0.4.6
google-pasta==0.2.0
graphviz==0.19.1
grpcio==1.43.0
h5py==2.10.0
idna==3.3
importlib-metadata==4.10.0
importlib-resources==5.4.0
ipykernel==6.6.1
ipython==7.31.0
ipython-genutils==0.2.0
ipywidgets==7.6.5
itsdangerous==2.1.1
jedi==0.18.1
Jinja2==3.0.3
joblib==1.1.0
jsonschema==4.3.3
jupyter-client==7.1.0
jupyter-core==4.9.1
jupyterlab-pygments==0.1.2
jupyterlab-widgets==1.0.2
kaleido==0.2.1
Keras==2.4.3
Keras-Preprocessing==1.1.2
kiwisolver==1.3.2
libclang==12.0.0
Markdown==3.3.6
MarkupSafe==2.0.1
matplotlib==3.3.1
matplotlib-inline==0.1.3
memory-profiler==0.60.0
mimesis==4.1.3
mistune==0.8.4
mypy==0.782
mypy-extensions==0.4.3
nbclient==0.5.9
nbconvert==6.4.0
nbformat==5.1.3
nest-asyncio==1.5.4
notebook==6.4.6
numpy==1.21.5
oauthlib==3.1.1
opt-einsum==3.3.0
packaging==21.3
pandas==1.1.2
pandocfilters==1.5.0
parso==0.8.3
pickle5==0.0.12
pickleshare==0.7.5
pigar==0.10.0
Pillow==8.3.2
pipreqs==0.4.11
plotly==5.6.0
prometheus-client==0.12.0
prompt-toolkit==3.0.24
protobuf==3.19.1
psutil==5.9.0
pyaml==21.10.1
pyasn1==0.4.8
pyasn1-modules==0.2.8
pycparser==2.21
pydot==1.4.2
Pygments==2.11.2
pyorbital==1.7.1
pyparsing==3.0.6
pyrsistent==0.18.0
pyscagnostics==0.1.0a4
python-dateutil==2.8.2
pytz==2021.3
pytz-deprecation-shim==0.1.0.post0
pywin32==303
pywinpty==1.1.6
PyYAML==6.0
pyzmq==22.3.0
requests==2.27.0
requests-oauthlib==1.3.0
rpy2==3.4.5
rsa==4.8
scikit-learn==0.23.2
scikit-optimize==0.9.0
scipy==1.4.1
seaborn==0.11.0
Send2Trash==1.8.0
six==1.16.0
sklearn==0.0
tenacity==8.0.1
tensorboard==2.7.0
tensorboard-data-server==0.6.1
tensorboard-plugin-wit==1.7.0
tensorflow==2.3.3
tensorflow-addons==0.11.2
tensorflow-estimator==2.3.0
tensorflow-io-gcs-filesystem==0.23.1
termcolor==1.1.0
terminado==0.12.1
testpath==0.5.0
threadpoolctl==3.0.0
tornado==6.1
tqdm==4.62.3
traitlets==5.1.1
typed-ast==1.4.3
typeguard==2.13.3
typing_extensions==4.0.1
tzdata==2021.5
tzlocal==4.1
urllib3==1.26.7
wcwidth==0.2.5
webencodings==0.5.1
Werkzeug==2.0.2
widgetsnbextension==3.5.2
wrapt==1.13.3
yarg==0.1.9
zipp==3.7.0
